# Backend Programming Challenge

Welcome to the WebTec Solutions programming challenge.

## Objective

The goal of the challenge is to build a RESTful API service for a TODO list app using [Laravel](https://laravel.com/).

### Features

- Adding a new TODO item
- Marking a TODO item as completed
- Listing all TODO items
- Listing TODO items not completed yet
- TODO items are stored in a database

### Nice to haves

- +1 for adding tests
- +1 for dockerizing your app
- +1 for providing documentation for your API
- +1 for describing the infrastructure needed to deploy to a cloud hosting provider

## Submission

Put your submission on your GitHub, GitLab, Bitbucket, etc. account as a public repository and send the link back to us.

Good luck! :)
